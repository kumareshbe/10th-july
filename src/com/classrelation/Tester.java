package com.classrelation;

import java.util.ArrayList;
import java.util.List;

public class Tester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		LibraryManagementSystem libSystem=new LibraryManagementSystem();
		
		
		
		List<String> sections=new ArrayList<String>();
		sections.add("ece");
		sections.add("cse");

		
		Department dep1=new Department(50,sections,3);
		Department dep2=new Department(23,sections,4);
		
		
		List depList=new ArrayList<Department>();
		depList.add(dep1);
		depList.add(dep2);
		
		
		
		Book book1=new Book("Bansal", "Fluid Mechanics", 1197, false);
		Book book2=new Book("Vijay Ragavan","Maths", 500, true);
		
		List bookList=new ArrayList<Book>();
		bookList.add(book1);
		bookList.add(book2);
		
		
		Library lib=new Library("College", "Velachery", bookList);
		lib.setDepList(depList);
		
		
		libSystem.lib=lib;
		try 
		{
			System.out.println(libSystem.getBookStatus("Physics"));
		} 
		catch (BookNotFoundException e)
		{
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	
		try 
		{
			Book result=libSystem.getBookDetails("Maths");
			System.out.println(result.getBookName()+" "+result.getAuthorName());
		} catch (BookNotFoundException e)
		{
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		
		
		
		
		
		
	}

}
