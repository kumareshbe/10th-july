package com.classrelation;

public class Book 

{
	private String authorName;
	private String bookName;
	private int pageNos;
	private boolean status;
	
	
	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Book(String authorName, String bookName, int pageNos,boolean status) {
		super();
		this.authorName = authorName;
		this.bookName = bookName;
		this.pageNos = pageNos;
		this.status=status;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public int getPageNos() {
		return pageNos;
	}
	public void setPageNos(int pageNos) {
		this.pageNos = pageNos;
	}
	
	public boolean getStatus()
	{
		return status;
	}
	
	public void setStatus(boolean status)
	{
		this.status=status;
	}
	

}
