package com.classrelation;

public class LibraryManagementSystem 
{

	Library lib=new Library();
	
	public boolean getBookStatus(String bookName) throws BookNotFoundException
	{
		int flag=0;
		for(Book bookEach:lib.getBookList())
			{
				if (bookEach.getBookName().equalsIgnoreCase(bookName))
				{
					flag=flag+1;
					return bookEach.getStatus();
				}
			
			}
		
		if(flag==0)
		{
			throw new BookNotFoundException(bookName);
		}
		return false;	
		
	}
	
	public Book getBookDetails(String bookName) throws BookNotFoundException
	{
		int flag=0;
		for(Book bookEach:lib.getBookList())
		{
			
			if (bookEach.getBookName().equalsIgnoreCase(bookName))
			{
				flag=flag+1;
				return bookEach;
			}
		}
		if(flag==0)
		{
			throw new BookNotFoundException(bookName);
		}
		
		
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
}
