package com.classrelation;

import java.util.List;

public class Department 
{
	private int chairs;
	private List<String> sections;
	private int floorNo;
	
	public Department() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Department(int chairs, List<String> sections, int floorNo) {
		super();
		this.chairs = chairs;
		this.sections = sections;
		this.floorNo = floorNo;
	}
	public int getChairs() {
		return chairs;
	}
	public void setChairs(int chairs) {
		this.chairs = chairs;
	}
	public List<String> getSections() {
		return sections;
	}
	public void setSections(List<String> sections) {
		this.sections = sections;
	}
	public int getFloorNo() {
		return floorNo;
	}
	public void setFloorNo(int floorNo) {
		this.floorNo = floorNo;
	}
	
	
	
	
	
	
	
	
}
