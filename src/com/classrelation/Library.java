package com.classrelation;

import java.util.List;

public class Library
{

	private String name;
	private String address;
	private List<Department> depList;
	private List<Book> bookList;
	
	public Library() 
	{
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Department> getDepList() {
		return depList;
	}

	public void setDepList(List<Department> depList) {
		this.depList = depList;
	}

	public List<Book> getBookList() {
		return bookList;
	}

	public void setBookList(List<Book> bookList) {
		this.bookList = bookList;
	}

	public Library(String name, String address, List<Book> bookList) {
		super();
		this.name = name;
		this.address = address;
		this.bookList = bookList;
	}
	
}
