package com.classrelation;

public class BookNotFoundException extends Exception
{
	 private String bookName;
	
	public BookNotFoundException(String bookName)
	{
		this.bookName=bookName;
	}
	
	
	
	
	public String getMessage()
	{
		return "The book with the name "+ bookName +" is not found";
	}
	
}
